## Build:

- mvn jfx:run to just run the app
- mvn jfx:jar creates an executable jar
- mvn jfx:native creates a full packaged app with jdk and installer

## Usage:

### Tab1 - TextAnalyzer:
- Copy text into the text area or load a text file
- Press the button analyze to create a dictionary
- Press Draw Graph to show statistics about your usage of different words

### Tab2 - SymbolCounter:
- Copy text into the text area
- Count symbols/words with named buttons
- Count occurrences of symbols or words with the other text fields

## Changelog:

### v1.0
- Renamed to TextAnalyzer
- Refactored this project (WordCounter)
- Included old project SymbolCounter in separate Tab
- Deleted old repos
