package de.artismarti.textanalyzer.controller;

import de.artismarti.textanalyzer.Main;
import de.artismarti.textanalyzer.logic.Counter;
import de.artismarti.textanalyzer.logic.Dictionary;
import de.artismarti.textanalyzer.util.MapUtils;
import de.artismarti.textanalyzer.util.Strings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.Axis;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller for text analyzer and symbol counter tab.
 *
 * @author artur
 */
public class Controller implements Initializable {

	private final String findings = "Findings: ";

	// Tab text analyzer
	@FXML
	public Text amountWords;
	@FXML
	private TextArea textArea;

	private Dictionary dict;

	// Tab symbol counter
	@FXML
	private TextArea textAreaTab2;
	@FXML
	public Text amountSymbols;
	@FXML
	public Text amountWords2;
	@FXML
	public Text symbolFindings;
	@FXML
	public Text wordFindings;
	@FXML
	public TextField symbolTextField;
	@FXML
	public TextField wordTextField;

	private Counter counter;

	// Init
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		counter = new Counter();
	}

	/*
	 * Tab 1: TextAnalyzer
	 */

	@FXML
	public void analyze() {
		String text = textArea.getText();
		dict = Dictionary.of(text);
		countWords();
	}

	private void countWords() {
		amountWords.setText(String.valueOf(dict.getDict().keySet().size()));
	}

	@FXML
	public void selectFile() {
		textArea.setText("");
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Choose a txt file...");
		Optional<File> file = Optional.ofNullable(chooser.showOpenDialog(Main.window));
		if (file.isPresent()) {
			Path path = file.get().toPath();
			dict = Dictionary.of(path);
			countWords();
			try {
				List<String> lines = Files.readAllLines(path);
				for (String line : lines) {
					textArea.appendText(line + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@FXML
	public void draw() {
		if (dict == null) {
			analyze();
		}
		buildGraph();
	}

	private void buildGraph() {
		Stage stage = new Stage();
		final Axis<String> xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis(1, MapUtils.getHighestValue(dict.getDict()), 1);
		final LineChart<String, Number> lineChart = new LineChart<>(xAxis, yAxis);
		XYChart.Series<String, Number> series = new XYChart.Series<>();
		series.setName("Most used words:");
		fillData(series);
		Scene scene = new Scene(lineChart, 800, 600);
		lineChart.getData().add(series);
		stage.setScene(scene);
		stage.show();
	}

	private void fillData(XYChart.Series<String, Number> series) {
		LinkedHashMap<String, Integer> sortedMap = MapUtils.sort(dict.getDict());
		for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
			series.getData().add(new XYChart.Data<>(entry.getKey(), entry.getValue()));
		}
	}

	/*
	 * Tab 2: SymbolCounter
	 */

	@FXML
	public void countSymbols() {
		int count = counter.countSymbols(textAreaTab2.getText());
		amountSymbols.setText("#Symbols: " + count);
	}

	@FXML
	public void countWordsTab2() {
		int count = counter.countWords(textAreaTab2.getText());
		amountWords2.setText("#Words: " + count);
	}

	@FXML
	public void countSymbolInText() {
		final String text = symbolTextField.getText();
		if (Strings.notEmpty(text)) {
			final char c = text.charAt(0);
			int symbols = counter.countSymbolInText(c, textAreaTab2.getText());
			symbolFindings.setText(findings + symbols);
		}
	}

	@FXML
	public void countWordInText() {
		final String word = wordTextField.getText();
		if (Strings.notEmpty(word)) {
			int words = counter.countWordInText(word, textAreaTab2.getText());
			wordFindings.setText(findings + words);
		}
	}

}
