package de.artismarti.textanalyzer.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.nio.file.Files;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Symbol counter can perform on strings or files to count symbols/words.
 * It can also count the amount of a specific symbol/word in a text.
 *
 * @author artur
 */
public class Counter {

	/**
	 * Counts symbols from a string.
	 *
	 * @param text a string
	 * @return amount of symbols
	 */
	public int countSymbols(final String text) {
		return text.length();
	}

	/**
	 * Counts symbols from a file.
	 *
	 * @param file to read from
	 * @return amount of symbols
	 */
	public int countSymbols(final File file) {
		if (file == null) {
			return 0;
		}

		int result = 0;

		try {

			List<String> lines = Files.readAllLines(file.toPath());
			result = lines.stream()
					.mapToInt(String::length)
					.sum();

		} catch (final IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Counts all words in a text.
	 *
	 * @param text text to count words from
	 * @return amount of words
	 */
	public int countWords(final String text) {
		if (text == null || text.equals("")) {
			return 0;
		}

		int result = 0;

		final StringTokenizer st = new StringTokenizer(text);
		while (st.hasMoreTokens()) {
			st.nextToken();
			result++;
		}

		return result;
	}

	/**
	 * Counts all words from a file.
	 *
	 * @param file to read from
	 * @return amount of words
	 */
	public int countWords(final File file) {
		if (file == null) {
			return 0;
		}

		int result = 0;
		try {
			final StreamTokenizer st = new StreamTokenizer(new FileReader(file));

			int tokenType;
			while ((tokenType = st.nextToken()) != StreamTokenizer.TT_EOF) {
				if (tokenType == StreamTokenizer.TT_WORD) {
					result++;
				}
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Counts a specific symbol in a string
	 *
	 * @param symbol to count
	 * @param text   to search in
	 * @return amount of appearence of the symbol
	 */
	public int countSymbolInText(final char symbol, final String text) {
		if (text == null || text.equals("")) {
			return 0;
		}

		int result = 0;

		for (final char c : text.toCharArray()) {
			if (c == symbol) {
				result++;
			}
		}

		return result;
	}

	/**
	 * Counts a specific symbol in a string
	 *
	 * @param symbol to count
	 * @param file   to search in
	 * @return amount of appearence of the symbol
	 */
	public int countSymbolInText(final char symbol, final File file) {
		if (file == null) {
			return 0;
		}

		int result = 0;

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String line;
			while ((line = br.readLine()) != null) {
				result = result + countSymbolInText(symbol, line);
			}

		} catch (final IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Counts a specific word in a string
	 *
	 * @param word to count
	 * @param text to search in
	 * @return amount of appearence of the word
	 */
	public int countWordInText(final String word, final String text) {
		if (text == null || text.equals("")) {
			return 0;
		}

		int result = 0;

		final StringTokenizer st = new StringTokenizer(text);
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.endsWith(".") || token.endsWith(",") || token.endsWith("!") || token.endsWith("?") ||
					token.endsWith(":") || token.endsWith(";")) {
				token = token.replaceAll("[.,:;?!]+", "");
				System.out.println(token);
			}
			if (word.equals(token)) {
				result++;
			}
		}

		return result;
	}

	/**
	 * Counts a specific word in a string
	 *
	 * @param word to count
	 * @param file to search in
	 * @return amount of appearence of the word
	 */
	public int countWordInText(final String word, final File file) {
		if (file == null) {
			return 0;
		}

		int result = 0;
		try {
			final StreamTokenizer st = new StreamTokenizer(new FileReader(file));

			int tokenType;
			while ((tokenType = st.nextToken()) != StreamTokenizer.TT_EOF) {
				if (tokenType == StreamTokenizer.TT_WORD) {
					if (word.equals(st.sval)) {
						result++;
					}
				}
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
