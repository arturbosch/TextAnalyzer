package de.artismarti.textanalyzer.logic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Counts the frequencies of words in an given text and stores them in a map.
 * <p>
 * Created by artur on 09.10.15.
 */
public class Dictionary {

	public static final String SPACE = " ";
	private Map<String, Integer> dict;

	private Dictionary() {
		dict = new HashMap<>();
	}

	public static Dictionary of(Path pathToFile) {
		Dictionary dict = new Dictionary();
		dict.createDictOfTextFile(pathToFile);
		return dict;
	}

	public static Dictionary of(String text) {
		Dictionary dict = new Dictionary();
		dict.createDictOfText(text);
		return dict;
	}

	public void createDictOfText(String text) {
		analyzeLine(text);
	}

	public void createDictOfTextFile(Path file) {
		try {
			List<String> lines = Files.readAllLines(file);
			lines.forEach(this::analyzeLine);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void analyzeLine(String line) {
		if (line.isEmpty()) {
			return;
		}
		String preparedLine = line.replaceAll("[.,;:?!-*()&%$#@/{}+0-9\\s\\\\]+", SPACE);
		StringTokenizer tokenizer = new StringTokenizer(preparedLine, SPACE);
		String token;
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			Integer value = dict.get(token);
			if (value == null) {
				dict.put(token, 1);
			} else {
				dict.put(token, value + 1);
			}
		}
	}

	public Map<String, Integer> getDict() {
		return dict;
	}
}
