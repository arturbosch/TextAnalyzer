package de.artismarti.textanalyzer.util;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Utils for hash maps.
 * <p>
 * Created by artur on 10.10.15.
 */
public class MapUtils {

	private MapUtils() {
	}

	/**
	 * Sorts the unsorted map by their value and returns the sorted map as a linked map.
	 *
	 * @param unsortedMap unsorted map
	 * @return sorted map
	 */
	public static LinkedHashMap<String, Integer> sort(Map<String, Integer> unsortedMap) {
		return unsortedMap.entrySet().stream()
				.sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(e1, e2) -> e1, LinkedHashMap::new));
	}

	/**
	 * Used to determine the y-axis for the graph. If no value in the map is present, the y-axis will be 2.
	 *
	 * @param map dictionary of word counts
	 * @return the highest value inside the map
	 */
	public static int getHighestValue(Map<String, Integer> map) {
		return map.entrySet().stream()
				.mapToInt(Map.Entry::getValue)
				.max()
				.orElseGet(() -> 2);
	}
}
