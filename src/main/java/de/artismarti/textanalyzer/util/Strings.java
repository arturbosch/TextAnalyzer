package de.artismarti.textanalyzer.util;

/**
 * @author artur
 */
public class Strings {

	private Strings() {
	}

	public static boolean notEmpty(String string) {
		return !(string == null || string.isEmpty());
	}
}
