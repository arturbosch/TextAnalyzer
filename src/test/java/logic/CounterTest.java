package logic;

import de.artismarti.textanalyzer.logic.Counter;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author artur
 */
public class CounterTest {

	private final static String ONE_LINE_FILE = "./src/test/resources/SingleLineText";
	private final static String MORE_LINE_FILE = "./src/test/resources/MoreLineText";

	private final static String TEXT_29 = "Hello, my name is Ollijasmus!";
	private final static int TEXT_COUNT = 5;
	private final static int TIMES_TEXT_29 = 4;

	private Counter counter;
	private File file_one;
	private File file_more;

	@Before
	public void setUp() {
		counter = new Counter();
		file_one = new File(ONE_LINE_FILE);
		file_more = new File(MORE_LINE_FILE);
	}

	@Test
	public void testCountSymbolsWithString() {
		assertTrue(counter.countSymbols(TEXT_29) == TEXT_29.length());
	}

	@Test
	public void testCountSymbolsWithFileOneLine() {
		assertTrue(counter.countSymbols(file_one) == TEXT_29.length());
	}

	@Test
	public void testCountSymbolsWithFileMoreLines() {
		assertThat(counter.countSymbols(file_more), is(TEXT_29.length() * TIMES_TEXT_29));
	}

	@Test
	public void testCountWordsWithString() {
		assertTrue(counter.countWords(TEXT_29) == 5);
	}

	@Test
	public void testCountWordsWithFile() {
		assertTrue(counter.countWords(file_one) == TEXT_COUNT);
	}

	@Test
	public void testCountWordsWithFileMoreLines() {
		assertTrue(counter.countWords(file_more) == TEXT_COUNT * TIMES_TEXT_29);
	}
}
