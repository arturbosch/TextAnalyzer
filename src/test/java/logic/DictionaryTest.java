package logic;

import de.artismarti.textanalyzer.logic.Dictionary;
import org.junit.Test;

import java.nio.file.Paths;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author artur
 */
public class DictionaryTest {

	private final static String TEST_FILE = "./src/test/resources/MoreLineText";
	private static final String OLLIJASMUS = "Ollijasmus";
	private final static String TEXT =
			" Hello, my name is Ollijasmus!" +
			" Hello, my name is Ollijasmus!" +
			" Hello, my name is Ollijasmus!" +
			" Hello, my name is Ollijasmus!";

	public static final int COUNT = 4;

	@Test
	public void testOfTextFile() throws Exception {
		Dictionary dict = Dictionary.of(Paths.get(TEST_FILE));

		assertThat(dict.getDict().get(OLLIJASMUS), is(COUNT));
	}

	@Test
	public void testOfText() throws Exception {
		Dictionary dict = Dictionary.of(TEXT);

		assertThat(dict.getDict().get(OLLIJASMUS), is(COUNT));
	}

}
